import 'package:flutter/material.dart';
import 'package:gangabox_pos/utils/dummy_data.dart';
import 'package:gangabox_pos/widgets/custom_navigation_rail.dart';

class PosContainer extends StatelessWidget {
  const PosContainer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget userData = Container(
        margin: const EdgeInsets.symmetric(horizontal: 10.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              DummyData.userName,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            Text(
              DummyData.role,
            ),
          ],
        ));

    Widget logoutButton = IconButton(
      icon: const Icon(Icons.logout),
      tooltip: 'Cerrar sesión',
      onPressed: () {
        debugPrint('logout button pressed');
      },
    );

    final appBar = AppBar(
        leadingWidth: 200.0,
        leading: const Image(
          image: AssetImage('assets/img/logo.webp'),
          fit: BoxFit.fitHeight,
          alignment: Alignment.topLeft,
        ),
        actions: <Widget>[
          userData,
          logoutButton,
        ]);

    return Scaffold(
      appBar: appBar,
      body: const CustomNavigationRail(),
    );
  }
}
