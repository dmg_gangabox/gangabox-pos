import 'package:flutter/material.dart';
import 'package:gangabox_pos/providers/loading_info.dart';
import 'package:gangabox_pos/screens/pos_screen/pos_screen.dart';
import 'package:provider/provider.dart';

class CustomNavigationRail extends StatefulWidget {
  const CustomNavigationRail({Key? key}) : super(key: key);

  @override
  State<CustomNavigationRail> createState() => _CustomNavigationRailState();
}

class _CustomNavigationRailState extends State<CustomNavigationRail> {
  int _selectedIndex = 0;

  onDestinationSelected(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  final List widgetsChildren = [
    const PosScreen(),
    const Center(child: Text('selectedIndex: 1')),
    const Center(child: Text('selectedIndex: 2')),
  ];

  @override
  Widget build(BuildContext context) {
    final loadingInfo = Provider.of<LoadingInfo>(context);

    return Scaffold(
      body: Row(
        children: <Widget>[
          NavigationRail(
            selectedIndex: _selectedIndex,
            onDestinationSelected: onDestinationSelected,
            labelType: NavigationRailLabelType.selected,
            destinations: <NavigationRailDestination>[
              NavigationRailDestination(
                icon: Tooltip(
                  child: Icon(Icons.point_of_sale),
                  message: 'POS',
                ),
                selectedIcon: Icon(Icons.point_of_sale),
                label: Text('POS'),
              ),
              NavigationRailDestination(
                icon: Tooltip(
                  child: Icon(Icons.insert_drive_file),
                  message: 'Pedidos',
                ),
                selectedIcon: Icon(Icons.insert_drive_file),
                label: Text('Pedidos'),
              ),
              NavigationRailDestination(
                icon: Tooltip(
                  child: Icon(Icons.account_circle),
                  message: 'Cliente',
                ),
                selectedIcon: Icon(Icons.account_circle),
                label: Text('Cliente'),
              ),
            ],
          ),
          const VerticalDivider(thickness: 1, width: 1),
          // This is the main content.
          Expanded(
            child: widgetsChildren[_selectedIndex],
          )
        ],
      ),
    );
  }
}
