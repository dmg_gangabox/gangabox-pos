import 'package:flutter/material.dart';
import 'package:gangabox_pos/providers/loading_info.dart';
import 'package:gangabox_pos/utils/custom_colors.dart';
import 'package:gangabox_pos/widgets/containers/pos_container.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // @override
  Widget build(BuildContext context) {
    return MultiProvider(
        // TODO: Implementar provider correctamente
        providers: [
          ChangeNotifierProvider(create: (_) => LoadingInfo()),
        ],
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Gangabox POS',
          theme: ThemeData(
            primarySwatch:
                CustomColors.createMaterialColor(CustomColors.brandColor),
          ),
          initialRoute: 'home',
          routes: {'home': (context) => const PosContainer()},
          // home: const PosContainer(),
        ));
  }
}
