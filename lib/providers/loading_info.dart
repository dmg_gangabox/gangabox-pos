import 'package:flutter/material.dart';

class LoadingInfo with ChangeNotifier {
  bool _loading = false;
  String _type = '';
  bool _hasError = false;
  String _error = '';

  setLoading({required bool loading, String? type}) {
    _loading = loading;
    _type = type ?? '';
    _hasError = false;
    _error = '';

    notifyListeners();
  }

  setError({String? error, String? type}) {
    _loading = false;
    _type = type ?? '';
    _hasError = true;
    _error = error ?? '';

    notifyListeners();
  }

  get loading {
    return _loading;
  }

  get type {
    return _type;
  }

  get hasError {
    return _hasError;
  }

  get error {
    return _error;
  }
}
