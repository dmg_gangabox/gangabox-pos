// To parse this JSON data, do
//
//     final getProductsResponse = getProductsResponseFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

GetProductsResponse getProductsResponseFromJson(String str) =>
    GetProductsResponse.fromJson(json.decode(str));

String getProductsResponseToJson(GetProductsResponse data) =>
    json.encode(data.toJson());

class GetProductsResponse {
  GetProductsResponse({
    required this.message,
    required this.total,
    required this.page,
    required this.items,
  });

  String message;
  int total;
  int page;
  List<Product> items;

  factory GetProductsResponse.fromJson(Map<String, dynamic> json) =>
      GetProductsResponse(
        message: json["message"],
        total: json["total"],
        page: json["page"],
        items:
            List<Product>.from(json["items"].map((x) => Product.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "message": message,
        "total": total,
        "page": page,
        "items": List<dynamic>.from(items.map((x) => x.toJson())),
      };
}

class Product {
  Product({
    required this.info,
    required this.productName,
    required this.productPrice,
    required this.priceCompare,
    required this.productImage,
    required this.productStatus,
    required this.productId,
    required this.productOrder,
  });

  Info info;
  String productName;
  double productPrice;
  double priceCompare;
  String productImage;
  int productStatus;
  int productId;
  int productOrder;

  factory Product.fromJson(Map<String, dynamic> json) => Product(
        info: Info.fromJson(json["info"]),
        productName: json["product_name"],
        productPrice: json["product_price"].toDouble(),
        priceCompare: json["price_compare"].toDouble(),
        productImage: json["product_image"],
        productStatus: json["product_status"],
        productId: json["product_id"],
        productOrder: json["product_order"],
      );

  Map<String, dynamic> toJson() => {
        "info": info.toJson(),
        "product_name": productName,
        "product_price": productPrice,
        "price_compare": priceCompare,
        "product_image": productImage,
        "product_status": productStatus,
        "product_id": productId,
        "product_order": productOrder,
      };
}

class Info {
  Info({
    required this.name,
    required this.description,
    required this.productId,
    required this.productStatus,
    required this.productPrice,
    required this.productPriceCompare,
  });

  String name;
  String description;
  String productId;
  int productStatus;
  double productPrice;
  double productPriceCompare;

  factory Info.fromJson(Map<String, dynamic> json) => Info(
        name: json["name"],
        description: json["description"],
        productId: json["product_id"],
        productStatus: json["product_status"],
        productPrice: json["product_price"].toDouble(),
        productPriceCompare: json["product_price_compare"].toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "description": description,
        "product_id": productId,
        "product_status": productStatus,
        "product_price": productPrice,
        "product_price_compare": productPriceCompare,
      };
}
