import 'package:gangabox_pos/models/get_products_model.dart';
import 'package:gangabox_pos/utils/constants.dart';
import 'package:http/http.dart' as http;

class ProductsService {
  static Future<GetProductsResponse> getProducts({
    String? name,
    String? categoryId,
    String? familyId,
    int size = 20,
    int page = 1,
  }) async {
    String queryParams = 'size=$size&page=$page';

    if (name != null) queryParams += '&name=$name';
    if (categoryId != null) queryParams += '&category_id=$categoryId';
    if (familyId != null) queryParams += '&family_id=$familyId';

    final String url = '${Constants.domain}/products?$queryParams';

    final res = await http.get(Uri.parse(url));

    return getProductsResponseFromJson(res.body);
  }
}
