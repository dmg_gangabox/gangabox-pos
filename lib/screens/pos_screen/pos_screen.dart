// import 'dart:convert';

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:gangabox_pos/models/get_products_model.dart';
// import 'package:gangabox_pos/providers/loading_info.dart';
import 'package:gangabox_pos/services/products_service.dart';
import 'package:gangabox_pos/utils/custom_colors.dart';
// import 'package:http/http.dart' as http;
// import 'package:provider/provider.dart';

class PosScreen extends StatefulWidget {
  const PosScreen({Key? key}) : super(key: key);

  @override
  _PosScreenState createState() => _PosScreenState();
}

class _PosScreenState extends State<PosScreen> {
  late bool _hasMoreProducts;
  late int _productsPage;
  late String _searchText;
  late bool _getProductsrror;
  late bool _loadingGetProducts;
  final int _defaultProductsPerPageCount = 25;
  late List<Product> _products;

  Timer? timer;

  @override
  void initState() {
    super.initState();
    _hasMoreProducts = true;
    _productsPage = 1;
    _getProductsrror = false;
    _loadingGetProducts = true;
    _searchText = '';
    _products = [];
    fetchProducts();
  }

  Future<void> fetchProducts() async {
    setState(() {
      _loadingGetProducts = true;
    });
    try {
      final fetchedProducts = await ProductsService.getProducts(
          size: _defaultProductsPerPageCount,
          page: _productsPage,
          name: _searchText);
      setState(() {
        _hasMoreProducts =
            fetchedProducts.items.length == _defaultProductsPerPageCount;
        _loadingGetProducts = false;
        if (_productsPage == 1) {
          _products = fetchedProducts.items;
        } else {
          _products.addAll(fetchedProducts.items);
        }
        _productsPage += 1;
      });
    } catch (e) {
      debugPrint('Error: $e');
      setState(() {
        _loadingGetProducts = false;
        _getProductsrror = true;
      });
    }
  }

  _onSearchTextChanged(text) {
    setState(() {
      _searchText = text;
      _productsPage = 1;
    });

    timer?.cancel();

    timer = Timer(const Duration(seconds: 1), () => fetchProducts());
  }

  @override
  Widget build(BuildContext context) {
    // final productStream = new StreamController<List<Product>>();

    // @override
    // void dispose() {
    //   productStream.close();
    //   super.dispose();
    // }

    Widget searchBar = Container(
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(5.0)),
      margin: const EdgeInsets.all(5.0),
      child: TextField(
        // enabled: !_loadingGetProducts,
        onChanged: _onSearchTextChanged,
        decoration: InputDecoration(
            border: const OutlineInputBorder(),
            hintText: 'Buscar productos',
            suffixIcon: _loadingGetProducts
                ? Transform.scale(
                    scale: 0.5,
                    child: CircularProgressIndicator(),
                  )
                : Icon(Icons.search)),
      ),
    );

    _productList() {
      if (_products.isEmpty) {
        if (_loadingGetProducts) {
          return Center(child: CircularProgressIndicator());
        } else if (_getProductsrror) {
          return NetworkErrorMessage();
        } else if (_products.isEmpty) {
          return SearchErrorMessage();
        }
      } else if (_getProductsrror) {
        return NetworkErrorMessage();
      } else {
        return ProductList(
          products: _products,
          fetchProducts: fetchProducts,
          loading: _loadingGetProducts,
          hasMore: _hasMoreProducts,
        );
      }

      return Container();
    }

    // Widget _productList = FutureBuilder(
    //   future: ProductsService.getProducts(),
    //   builder:
    //       (BuildContext context, AsyncSnapshot<GetProductsResponse> snapshot) {
    //     if (snapshot.connectionState == ConnectionState.waiting) {
    //       return Center(child: CircularProgressIndicator());
    //     } else if (snapshot.hasError) {
    //       x
    //     }
    //     return ProductList(snapshot.data?.items ?? []);
    //   },
    // );

    // _productList() {
    //   return StreamBuilder(
    //     // stream: ProductsService.getProducts(),
    //     stream: productStream.stream,
    //     builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
    //       if (!snapshot.hasData) {
    //         ProductsService.getProducts();
    //         return Center(child: CircularProgressIndicator());
    //       }

    //       return Container();
    //     },
    //   );
    // }

    return Stack(
      // children: [_productList(), searchBar],
      children: [_productList(), searchBar],
    );
  }
}

class NetworkErrorMessage extends StatelessWidget {
  const NetworkErrorMessage({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(
            Icons.error,
            color: Colors.redAccent,
            size: 50,
          ),
          Text('Ha ocurrido un error, por favor intenta nuevamente.')
        ],
      ),
    );
  }
}

class SearchErrorMessage extends StatelessWidget {
  const SearchErrorMessage({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Image(
        image: AssetImage(
          'assets/img/search-failed.png',
        ),
        height: MediaQuery.of(context).size.height * 0.5,
        width: MediaQuery.of(context).size.width * 0.8,
      ),
    );
  }
}

class ProductList extends StatelessWidget {
  final List<Product> products;
  final VoidCallback fetchProducts;
  final bool loading;
  final bool hasMore;

  ProductList(
      {Key? key,
      required this.products,
      required this.fetchProducts,
      required this.loading,
      this.hasMore = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget _getMoreButton = Container(
      child: Card(
        elevation: 4.0,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5.0),
            side: const BorderSide(width: 1, color: Colors.grey)),
        child: ClipRRect(
            borderRadius: BorderRadius.circular(5.0),
            // child: Container(
            //     color: CustomColors.brandColor,
            child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                    // textStyle: const TextStyle(
                    //   fontSize: 16.0,
                    //   color: Colors.white,
                    //   fontWeight: FontWeight.bold,
                    // ),
                    ),
                onPressed: loading
                    ? null
                    : () {
                        fetchProducts();
                      },
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    loading
                        ? CircularProgressIndicator(
                            color: Colors.white,
                          )
                        : Icon(
                            Icons.add,
                            color: Colors.white,
                            size: 50.0,
                          ),
                    loading
                        ? Container()
                        : Text(
                            'Mostrar más productos',
                            textAlign: TextAlign.center,
                          )
                  ],
                ))),
      ),
      // ),
      decoration: BoxDecoration(
        boxShadow: const [
          BoxShadow(
            color: Colors.white,
            blurRadius: 5.0,
            offset: Offset(0, 10),
            spreadRadius: 0.5,
          ),
        ],
        borderRadius: BorderRadius.circular(12),
      ),
    );

    return CustomScrollView(
      slivers: <Widget>[
        const SliverToBoxAdapter(child: SizedBox(height: 60)),
        SliverGrid(
            delegate:
                SliverChildBuilderDelegate((BuildContext context, int index) {
              if (index == products.length) {
                return hasMore ? _getMoreButton : Container();
              }
              return ProductItem(product: products[index]);
            }, childCount: products.length + 1),
            gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                maxCrossAxisExtent: 180,
                childAspectRatio: 1 / 1.6,
                crossAxisSpacing: 5.0,
                mainAxisSpacing: 5.0))
      ],
    );
  }
}

class ProductItem extends StatelessWidget {
  final Product product;

  const ProductItem({
    Key? key,
    required this.product,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final discount = product.productPrice < product.priceCompare;

    Widget _renderProduct = Container(
      color: Colors.white,
      child: Column(
        children: [
          ProductImage(product: product),
          Row(
            textBaseline: TextBaseline.alphabetic,
            crossAxisAlignment: CrossAxisAlignment.baseline,
            children: [
              Container(
                margin: const EdgeInsets.all(5.0),
                child: Text(
                  '\$${product.productPrice.toString()}',
                  style: TextStyle(
                      color: CustomColors.brandColor,
                      fontSize: 18.0,
                      fontWeight: FontWeight.bold),
                ),
              ),
              discount
                  ? Text(
                      '\$${product.priceCompare.toString()}',
                      style: TextStyle(
                          decoration: TextDecoration.lineThrough,
                          color: Colors.grey),
                    )
                  : Container(),
            ],
          ),
          Expanded(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 5.0),
              child: Center(
                child: Text(
                  product.productName,
                  overflow: TextOverflow.ellipsis,
                  // textAlign: TextAlign.center,
                  maxLines: 3,
                  // style: TextStyle(),
                ),
              ),
            ),
          )
        ],
      ),
    );

    return Container(
      child: Card(
        elevation: 4.0,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5.0),
            side: BorderSide(width: 1, color: Colors.grey)),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(5.0),
          child: discount
              ? Banner(
                  message: 'OFERTA',
                  location: BannerLocation.topEnd,
                  child: _renderProduct,
                )
              : _renderProduct,
        ),
      ),
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Colors.white,
            blurRadius: 5.0,
            offset: Offset(0, 10),
            spreadRadius: 0.5,
          ),
        ],
        borderRadius: BorderRadius.circular(12),
      ),
    );
  }
}

class ProductImage extends StatelessWidget {
  final Product product;
  int width;
  int height;

  ProductImage({
    Key? key,
    required this.product,
    this.width = 300,
    this.height = 300,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Image.network(
      'https://ik.imagekit.io/d24auiapgzh/images/${product.productImage}?tr=w-$width,h-$height',
      // loadingBuilder: (BuildContext context, Widget child,
      //     ImageChunkEvent? loadingProgress) {
      //   if (loadingProgress == null) {
      //     return child;
      //   }
      //   return CircularProgressIndicator(
      //       value: loadingProgress.expectedTotalBytes != null
      //           ? loadingProgress.cumulativeBytesLoaded /
      //               loadingProgress.expectedTotalBytes!
      //           : null,
      //       );
      // },
    );
  }
}
